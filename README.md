# onboarding-ai-bot

# Install Open AI Python 

To install the official Python bindings, run the following command:
```pip install openai```

# Run Open AI Python
To run the script type following command:
```python onboard.py```
