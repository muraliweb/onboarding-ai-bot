from openai import OpenAI

# Set your API key
client = OpenAI(api_key="sk-proj-dOGzHWgmSVrnXg7OW2PHT3BlbkFJRDJFtbgAHmqlVR90Zy9k")

def get_response(prompt):
  # Create a request to the chat completions endpoint
  response = client.chat.completions.create(
    model="gpt-4o",
    # Assign the role and content for the message
    messages=[{"role": "user", "content": prompt}], 
    temperature = 0)
  return response.choices[0].message.content

# Test the function with your prompt
response = get_response("Who is the CEO of Natwest?")
print(response)